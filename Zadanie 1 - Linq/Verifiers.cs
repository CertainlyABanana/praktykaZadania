using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

namespace Zadanie_1___Linq
{
    public class Verifiers
    {
        public static void A(List<Data> result)
        {
            var solution = new List<Data>()
            {
                new() { Number = 11, Char = "hij" },
                new() { Number = 6, Char = "klm" },
                new() { Number = 7, Char = "nop" }
            };
            
            result.Should().BeEquivalentTo(solution);
        }
        
        public static void B(int result)
        {
            result.Should().Be(8);
        }

        public static void C(List<Data> result)
        {
            string example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

            var solution = example
                .GroupBy(x => x)
                .Select(x => new Data() { Number = x.Count(), Char = x.Key.ToString() })
                .ToList();

            result.Should().BeEquivalentTo(solution);
        }
        
        public static void D(int[] result)
        {
            int[] solution = { 6, 8, 2, 4, 42, 4234, 10, 12 };
            
            result.Should().BeEquivalentTo(solution);
        }
        
        public static void E(IEnumerable<string> result)
        {
            IEnumerable<string> solution = new[] {"agdhx", "aweex" };
            
            result.Should().BeEquivalentTo(solution);
        }        
        
        public static void F(string result)
        {
            result.Should().BeEquivalentTo("banan");
        }
    }
}