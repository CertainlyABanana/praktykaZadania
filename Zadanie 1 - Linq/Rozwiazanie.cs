using System.Collections.Generic;
using System.Linq;

namespace Zadanie_1___Linq.Solution
{
    public class Rozwiazanie
    {
        // Znajdz wszystkie obiekty, gdzie Number > 5
        private static void ZadA()
        {
            List<Data> example = new()
            {
                new Data() {Number = 5, Char = "abc"},
                new Data() {Number = 2, Char = "efg"},
                new Data() {Number = 11, Char = "hij"},
                new Data() {Number = 6, Char = "klm"},
                new Data() {Number = 7, Char = "nop"}
            };

            List<Data> result = example.Select(x => x).Where(x => x.Number > 5).ToList();

            Verifiers.A(result);
        }
        
        // oblicz ilosc wyrazow
        private static void ZadB()
        {
            string example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

            int result = example.Split().Length;

            Verifiers.B(result);
        }
        
        // wpisac do Data ilosc wystapien znakow
        // char = dany znak
        // Number = ilosc wystapien danego znaku
        // wynikiem powinna byc List<Data>
        private static void ZadC()
        {
            string example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

            var result = example.GroupBy(x => x).Select(x => new Data() {Number = x.Count(), Char = x.Key.ToString()}).ToList();

            Verifiers.C(result);
        }
        
        // zwroc tylko parzyste w formie tablicy
        private static void ZadD()
        {
            int[] example = { 5, 6, 8, 2, 45, 7, 4, 35, 5, 42, 4234, 23, 47, 10, 3, 12 };

            int[] result = example.Select(x => x).Where(y => y % 2 == 0).ToArray();
            
            Verifiers.D(result);
        }
        
        // wybierz z kolekcji wyrazy, ktore rozpoczynaja sie tylko znakiem A, a koncza na X
        private static void ZadE()
        {
            string[] example = { "saidfg", "fsdh", "fsddj", "agdhx", "gjhrt", "aweex", "hjkds"};

            IEnumerable<string> result =
                example
                    .Select(x => x)
                    .Where(y => y.StartsWith("a"))
                    .Where(z => z.EndsWith("x"));
            
            Verifiers.E(result);
        }
        
        // stworz jeden string z wszystkich obiektow(a wlasciwie pola Char z tych obiektow,
        // w ktorych number jest nieparzysty
        // dopisz rozszerzenie do join(w LinqExtensions ponizej, tak aby nie trzeba bylo uzywac string.Join(...),
        // a zamiast tego wrzucic to od razu do linq
        private static void ZadF()
        {
            List<Data> example = new()
            {
                new Data() {Number = 12, Char = "abc"},
                new Data() {Number = 2, Char = "efg"},
                new Data() {Number = 11, Char = "ba"},
                new Data() {Number = 6, Char = "klm"},
                new Data() {Number = 7, Char = "nan"}
            };

            var result = example.Where(x => x.Number % 2 != 0).Select(x => x.Char).Join_DoNotUse("");

            Verifiers.F(result);
        }
    }
    
    public static class LinqExtensions2
    {
        public static string Join_DoNotUse(this IEnumerable<string> enumerable, string delimiter)
        {
            return string.Join(delimiter, enumerable);
        }
    }
}