using Zadanie_2___DI.Providers;

namespace Zadanie_2___DI
{
    public class Rozwiazanie
    {
        private IFileProvider _fileProvider;
        private IDateTimeProvider _dateTimeProvider;
        
        public Rozwiazanie(IFileProvider fileProvider, IDateTimeProvider dateTimeProvider)
        {
            _fileProvider = fileProvider;
            _dateTimeProvider = dateTimeProvider;
        }
        
        public bool DoWork()
        {
            string data = GetData();
           
            return data.Equals("good") && _dateTimeProvider.Now.Year < 2020;
        }

        public string GetData()
        {
            return _fileProvider.ReadAllText("");;
        }
    }
}