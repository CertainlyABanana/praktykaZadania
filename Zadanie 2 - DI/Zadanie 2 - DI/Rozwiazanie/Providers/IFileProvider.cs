namespace Zadanie_2___DI.Providers
{
    public interface IFileProvider
    {
        public string ReadAllText(string path);
    }
}