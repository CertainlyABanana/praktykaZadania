using System;

namespace Zadanie_2___DI.Providers
{
    public interface IDateTimeProvider
    {
        public DateTime Now { get; }
    }
}