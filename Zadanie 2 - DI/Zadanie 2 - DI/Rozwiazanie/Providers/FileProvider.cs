using System.IO;

namespace Zadanie_2___DI.Providers
{
    public class FileProvider : IFileProvider
    {
        public string ReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }
}