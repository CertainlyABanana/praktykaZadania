﻿using System;
using System.IO;

namespace Zadanie_2___DI
{
    
    /*
     *   przetestuj wszystkie sciezki metody DoWork klasy ExampleClass
     *   uzywajac do tego dependency inversion i injection
     *   uzyj xUnit/NUnit/MSTest
     *   rozbij na pliki + foldery
     */

    
    class Program
    {
        static void Main(string[] args)
        {
            ExampleClass obj = new();
            obj.DoWork();
        }
    }

    public class ExampleClass
    {
        public bool DoWork()
        {
            string data = GetData();
           
            return data.Equals("good") && DateTime.Now.Year < 2020;
        }

        public string GetData()
        {
            return File.ReadAllText("");;
        }
    }
}