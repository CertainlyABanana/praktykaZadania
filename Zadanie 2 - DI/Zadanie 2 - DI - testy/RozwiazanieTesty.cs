using System;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using Zadanie_2___DI;
using Zadanie_2___DI.Providers;

namespace Zadanie_2___DI___testy
{
    [TestFixture]
    public class RozwiazanieTesty
    {
        private IDateTimeProvider _dateTimeProvider = Substitute.For<IDateTimeProvider>();
        private IFileProvider _fileProvider = Substitute.For<IFileProvider>();
        private Rozwiazanie _rozwiazanie;
        
        [OneTimeSetUp]
        public void SetupTest()
        {
            _rozwiazanie = new Rozwiazanie(_fileProvider, _dateTimeProvider);
        }
        
        [Test]
        public void DoWork_PositivePath_ReturnsTrue()
        {
            _fileProvider.ReadAllText("").Returns("good");
            _dateTimeProvider.Now.Returns(new DateTime(1990, 1, 1));
            
            bool result = _rozwiazanie.DoWork();

            result.Should().BeTrue();
        }

        [Test]
        public void DoWork_DataGoodDateBad_ReturnsFalse()
        {
            _fileProvider.ReadAllText("").Returns("good");
            _dateTimeProvider.Now.Returns(new DateTime(2022, 1, 1));
            
            bool result = _rozwiazanie.DoWork();

            result.Should().BeFalse();
        }
        
        [Test]
        public void DoWork_DataBadDateGood_ReturnsFalse()
        {
            _fileProvider.ReadAllText("").Returns("foobar");
            _dateTimeProvider.Now.Returns(new DateTime(1990, 1, 1));
            
            bool result = _rozwiazanie.DoWork();

            result.Should().BeFalse();
        }
        
        [Test]
        public void DoWork_DataBadDateBad_ReturnsFalse()
        {
            _fileProvider.ReadAllText("").Returns("foobar");
            _dateTimeProvider.Now.Returns(new DateTime(2022, 1, 1));
            
            bool result = _rozwiazanie.DoWork();

            result.Should().BeFalse();
        }
    }
}